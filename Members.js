const members = [
    {
        id: 1,
        name: 'John Doe',
        email: 'john@doe.com',
        status: 'active'
    },
    {
        id: 2,
        name: 'Donald Duck',
        email: 'donald@duck.com',
        status: 'active'
    },
    {
        id: 3,
        name: 'Darth Vader',
        email: 'darth@vader.com',
        status: 'active'
    }
];

module.exports = members;